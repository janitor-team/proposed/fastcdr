fastcdr (1.0.25-1) unstable; urgency=medium

  * Update d/watch
  * New upstream version 1.0.25
  * Bump Standards-Version to 4.6.1

 -- Timo Röhling <roehling@debian.org>  Mon, 10 Oct 2022 21:37:58 +0200

fastcdr (1.0.24-1) unstable; urgency=medium

  * New upstream version 1.0.24

 -- Timo Röhling <roehling@debian.org>  Sat, 19 Mar 2022 23:39:39 +0100

fastcdr (1.0.23-1) unstable; urgency=medium

  * New upstream version 1.0.23

 -- Timo Röhling <roehling@debian.org>  Fri, 24 Dec 2021 09:02:02 +0100

fastcdr (1.0.22-2) unstable; urgency=medium

  * Transfer package to new Debian Robotics Team

 -- Timo Röhling <roehling@debian.org>  Sat, 09 Oct 2021 12:14:37 +0200

fastcdr (1.0.22-1) unstable; urgency=medium

  * New upstream version 1.0.22
  * Bump Standards-Version to 4.6.0 (no changes)

 -- Timo Röhling <roehling@debian.org>  Sat, 18 Sep 2021 00:05:04 +0200

fastcdr (1.0.21-2) unstable; urgency=medium

  * Upload to unstable.

 -- Timo Röhling <roehling@debian.org>  Sun, 15 Aug 2021 19:58:44 +0200

fastcdr (1.0.21-1) experimental; urgency=medium

  * New upstream version 1.0.21
  * Update maintainer info

 -- Timo Röhling <roehling@debian.org>  Wed, 02 Jun 2021 12:33:53 +0200

fastcdr (1.0.19-1) unstable; urgency=medium

  * New upstream version 1.0.19
  * Drop patch that was accepted upstream
    - 0004-Prevent-unexpected-length_error-exception.patch

 -- Timo Röhling <timo@gaussglocke.de>  Fri, 29 Jan 2021 16:12:28 +0100

fastcdr (1.0.18-2) unstable; urgency=medium

  * New patch to prevent unexpected length_error exception

 -- Timo Röhling <timo@gaussglocke.de>  Thu, 28 Jan 2021 22:59:42 +0100

fastcdr (1.0.18-1) unstable; urgency=medium

  * New upstream version 1.0.18
  * Drop patch 0004-Prevent-unaligned-access-on-SPARC-and-RISC-V.patch

 -- Timo Röhling <timo@gaussglocke.de>  Thu, 28 Jan 2021 18:50:26 +0100

fastcdr (1.0.17-2) unstable; urgency=medium

  * Prevent unaligned access on SPARC and RISC-V

 -- Timo Röhling <timo@gaussglocke.de>  Mon, 07 Dec 2020 17:18:59 +0100

fastcdr (1.0.17-1) unstable; urgency=medium

  * New upstream version 1.0.17
  * Drop patches that were accepted upstream
    - Improve long double serialization
    - Fix invalid memory access during array deserialization

 -- Timo Röhling <timo@gaussglocke.de>  Mon, 07 Dec 2020 14:08:31 +0100

fastcdr (1.0.16-2) unstable; urgency=medium

  * Source-only upload
  * Use optional regex to ignore all libstdc++ symbols

 -- Timo Röhling <timo@gaussglocke.de>  Fri, 27 Nov 2020 22:06:20 +0100

fastcdr (1.0.16-1) unstable; urgency=medium

  * Initial release. (Closes: #975627)

 -- Timo Röhling <timo@gaussglocke.de>  Tue, 24 Nov 2020 17:12:50 +0100
